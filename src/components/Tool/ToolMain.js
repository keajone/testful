// Module imports
import React from 'react';

// Component imports
import ProfileManager from '../Tabs/ProfileTabs';

// CSS imports
import '../css/Tool/ToolMain.css';

const ToolMain = () => (

    <div className="ToolMain">

        <div className="ToolMainWindow">
            <ProfileManager/>
        </div>

    </div>

);

export default ToolMain;